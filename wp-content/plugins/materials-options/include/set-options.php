<?php

function materials_prices_get_table_handle() {
    //объявить встроенную в WP переменную - дескриптор БД блога
    global $wpdb;
    //вернуть дескриптор таблицы настроек плагина, расположенной в БД WP-блога
    return $wpdb->prefix . "materials_prefs";
}

function type_wallpaper_get_table_handle() {
    //объявить встроенную в WP переменную - дескриптор БД блога
    global $wpdb;
    //вернуть дескриптор таблицы настроек плагина, расположенной в БД WP-блога
    return $wpdb->prefix . "type_prefs";
}

//Вызывается в момент активации плагина
function set_options() {
    global $wpdb;

    //Установить опции по умолчанию (они будут храниться в таблице настроек WP)
    add_option('materials_version', '0.42');
    //1. Будет ли плагин по умолчанию обрабатывать заголовки записей. 0 - нет
    add_option('materials_modify_title', 0);
    //То же для тела записей. 1 - да
    add_option('materials_modify_content', 1);


    //Вызов функции повторяется, т.к. данные действия происходят на этапе установки плагина,
    //когда вызов в теле еще не может быть осуществлён
    $materials_prices_prefs_table = materials_prices_get_table_handle();
    $type_wallpaper_prefs_table = type_wallpaper_get_table_handle();

    //Установить кодировку таблицы (пустая - использовать умолчальную кодировку MySQL
    $charset_collate = '';

    //Если версия MySQL не ниже указанной - установить кодировку для хранения
    //и сравнения как UTF-8
    if ( version_compare(mysql_get_server_info(), '4.1.0', '>=') )
        $charset_collate = "DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci";

    //Если в БД блога еще нёт таблицы настроек плагина - создать её.
    if($wpdb->get_var("SHOW TABLES LIKE '$materials_prices_prefs_table'") != $materials_prices_prefs_table) {
        $sql = "CREATE TABLE `" . $materials_prices_prefs_table . "` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `title` VARCHAR(255) NULL ,
            `price` INT NULL ,
            `parent_id` INT NULL ,
            `width` INT NULL ,
            `image` VARCHAR(255) NULL ,
            UNIQUE KEY id (id)
        )$charset_collate";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php'); //обращение к функциям WP для
        dbDelta($sql); //работы с БД. создаём новую таблицу
    }

    if($wpdb->get_var("SHOW TABLES LIKE '$type_wallpaper_prefs_table'") != $type_wallpaper_prefs_table) {
        $sql = "CREATE TABLE `" . $type_wallpaper_prefs_table . "` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `type` VARCHAR(255) NULL ,
            `price` INT NULL ,
            UNIQUE KEY id (id)
        )$charset_collate";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php'); //обращение к функциям WP для
        dbDelta($sql); //работы с БД. создаём новую таблицу
    }
} //Конец функции установки настроек плагина


function unset_options() {
    materials_prices_unset_options();
    type_wallpaper_unset_options();
}


function materials_prices_unset_options() {
    global $wpdb, $materials_prices_prefs_table;
    delete_option('materials_version');
    delete_option('materials_modify_title');
    delete_option('materials_modify_content');
    $sql = "DROP TABLE $materials_prices_prefs_table";
    $wpdb->query($sql);
}

function type_wallpaper_unset_options() {
    global $wpdb, $type_wallpaper_prefs_table;
    delete_option('materials_version');
    delete_option('materials_modify_title');
    delete_option('materials_modify_content');
    $sql = "DROP TABLE $type_wallpaper_prefs_table";
    $wpdb->query($sql);
}
