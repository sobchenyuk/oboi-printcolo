<?php
/*
 * Plugin Name: Материалы и цены
 */

register_activation_hook(__FILE__, 'set_options');

register_deactivation_hook(__FILE__, 'unset_options');



//require_once 'include/type-wallpaper.php';


function  gb_custom_js_admin(){
    echo <<<HTML
    <script >
    window.addEventListener('DOMContentLoaded',function() {
      var menu = document.querySelector('.toplevel_page_materials');
      if(menu){
         menu.querySelector('.toplevel_page_materials').setAttribute('href', '#');
         menu.querySelector('.wp-first-item').remove();
      }
    });
</script>
HTML;
}
add_action('admin_footer', 'gb_custom_js_admin');

add_action('admin_menu', function () {
    add_menu_page(
        'Материалы, цены',
        'Материалы, цены',
        'manage_options',
        'materials',
        'add_my_wallpaper',
        'dashicons-tag',
        200
    );
    add_submenu_page(
        'materials',
        'Редактирование типа обоев',
        'Тип обоев',
        'manage_options',
        'type-wallpaper',
        'options_menu'
    );
    add_submenu_page(
        'materials',
        'Редактирование вида материала',
        'Виды материалов',
        'manage_options',
        'materials-prices',
        'options_menu'
    );
});

require_once 'include/set-options.php';
$materials_prices_prefs_table = materials_prices_get_table_handle();
$type_wallpaper_prefs_table = type_wallpaper_get_table_handle();

require_once 'include/select.php';
require_once 'include/INSERT.php';


function options_menu()
{
    global $type_wallpaper_prefs_table;

    ?>
    <div class="wrap">
        <h1><?php echo get_admin_page_title() ?></h1>


        <?php

        ?>

        <div id="col-container" class="wp-clearfix">

            <div id="col-left">
                <div class="col-wrap">


                    <div class="form-wrap">
                        <h2>Добавить тип фотообоев</h2>
                        <form id="addtag" method="post" action="<? echo $_SERVER['REQUEST_URI'];?>" class="validate">
                            <div class="form-field form-required term-name-wrap">
                                <label for="type">Название</label>
                                <input name="type" id="type" type="text" value="" size="40" aria-required="true">
                                <p>Название определяет, как элемент будет отображаться на вашем сайте.</p>
                            </div>
                            <div class="form-field term-slug-wrap">
                                <label for="price">Цена</label>
                                <input name="price" id="price" type="number" value="" min="0" size="40">
                                <p>Цена определяет, какая стоимость у элемента будет отображаться на вашем сайте.</p>
                            </div>

                            <p class="submit">
                                <input type="submit" name="type_wallpaper" id="submit" class="button button-primary"
                                       value="Добавить новый тип"></p>
                        </form>
                    </div>

                </div>
            </div><!-- /col-left -->

            <div id="col-right">
                <div class="col-wrap">
                    <form id="posts-filter" method="post" action="<? echo $_SERVER['REQUEST_URI'];?>">

                            <div class="alignleft actions bulkactions">
                                <label for="bulk-action-selector-top" class="screen-reader-text">Выберите массовое действие</label><select name="action" id="bulk-action-selector-top">
                                    <option value="-1">Действия</option>
                                    <option value="delete">Удалить</option>
                                </select>
                                <input type="submit" id="doaction" class="button action" value="Применить">
                            </div>

                            <br class="clear">
                        </div>
                        <h2 class="screen-reader-text">Список меток</h2><table class="wp-list-table widefat fixed striped tags">
                            <thead>
                            <tr>
                                <td id="cb" class="manage-column column-cb check-column"><label
                                        class="screen-reader-text" for="cb-select-all-1">Выделить все</label><input
                                        id="cb-select-all-1" type="checkbox"></td>
                                <th scope="col" id="name" style="width: 400px;"
                                    class="manage-column column-name column-primary sortable desc">
                                    <a href="#">
                                        <span>Название</span>
                                        <span class="sorting-indicator"></span>
                                    </a>
                                </th>
                                <th scope="col" id="description" class="manage-column column-description sortable desc">
                                    <a href="#"><span>Цена</span>
                                        <span class="sorting-indicator"></span>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <tbody id="the-list" data-wp-lists="list:tag">

                            <?php

                            if(count(type_wallpaper_get_phrase($type_wallpaper_prefs_table)) !== 0) :
                            foreach (type_wallpaper_get_phrase($type_wallpaper_prefs_table) as $item => $value): ?>

                            <tr id="tag-<?php echo $value->id ?>">
                                <th scope="row" class="check-column">
                                    <label class="screen-reader-text" for="cb-select-192">
                                        Выбрать rty</label>
                                    <input type="checkbox"
                                           name="delete_tags[]"
                                           value="<?php echo $value->id ?>"
                                           id="cb-select-<?php echo $value->id ?>">
                                </th>
                                <td class="name column-name has-row-actions column-primary" data-colname="Название">

                                    <strong>
                                        <a class="row-title" href="<?php echo 'admin.php?page=type-wallpaper&edit-'.$value->id;?>">
                                            <?php echo $value->type ?>
                                        </a>
                                    </strong><br>


                                    <div class="row-actions">
                                        <span class="edit">
                                            <a href="<?php echo 'admin.php?page=type-wallpaper&edit-'.$value->id;?>">Изменить</a> |
                                        </span>

                                        <span
                                            class="delete">
                                            <a href="<?php echo 'admin.php?page=type-wallpaper&delete-' . $value->id;?>" class="delete-tag aria-button-if-js"
                                                role="button">Удалить</a>
                                        </span>

                                    </div>
                                    <button type="button" class="toggle-row">
                                        <span class="screen-reader-text">Показать больше деталей</span>
                                    </button>
                                </td>

                                <td class="description column-description" data-colname="Цена">
                                    <p>
                                        <?php echo $value->price ?>
                                    </p>
                            </tr>

                            <?php

                            endforeach;
                            else: ?>

                            <tr class="no-items"><td class="colspanchange" colspan="5">Фотообоев не найдено.</td></tr>	</tbody>

                            <?php  endif;?>

                            <tfoot>
                            <tr>
                                <td class="manage-column column-cb check-column"><label class="screen-reader-text"
                                                                                        for="cb-select-all-2">Выделить
                                        все</label><input id="cb-select-all-2" type="checkbox"></td>
                                <th scope="col" class="manage-column column-name column-primary sortable desc">
                                    <a href="#"><span>Название</span>
                                        <span class="sorting-indicator"></span>
                                    </a>
                                </th>
                                <th scope="col" class="manage-column column-description sortable desc">
                                    <a href="#">
                                        <span>Цена</span>
                                        <span class="sorting-indicator"></span>
                                    </a>
                                </th>
                            </tr>
                            </tfoot>

                        </table>
                        <div class="tablenav bottom">

                            <div class="alignleft actions bulkactions">
                                <label for="bulk-action-selector-bottom" class="screen-reader-text">Выберите массовое действие</label><select name="action2" id="bulk-action-selector-bottom">
                                    <option value="-1">Действия</option>
                                    <option value="delete">Удалить</option>
                                </select>
                                <input type="submit" id="doaction2" class="button action" value="Применить">
                            </div>

                            <br class="clear">
                        </div>

                    </form>


                </div>
            </div>

        </div>

    </div>
    <?php

}

