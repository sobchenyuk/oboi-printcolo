<?php if (comments_open()) { ?>
<div class="row">
  <div class="col-sm-12">
    <div class="form-horizontal">

<?php
      $fields = array(
        'author' => '<div class="col-md-6">
                      <div class="form-group">
                        <label for="user_name" class="col-sm-4 control-label">Имя:*</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="user_name" placeholder="Обязательное поле" required="required" name="author">
                        </div>
                      </div>',

      'email' => '<div class="form-group">
                    <label for="user_mail" class="col-sm-4 control-label">Email:*</label>
                    <div class="col-sm-8">
                    <input type="email" class="form-control" id="user_mail" placeholder="Обязательное поле" required="required" name="email">
                    </div>
                  </div>',
      'url' => '<div class="form-group">
                    <label for="user_url" class="col-sm-4 control-label">VK:</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="user_vk" placeholder="VK" name="url">
                    </div>
                  </div>',
      );

      $args = array(
        'comment_notes_after' => '',
        'comment_field' => '<div class="col-md-6">
                              <div class="form-group">
                                <label for="user_mail" class="col-sm-4 control-label">Комментарий:</label>
                                <div class="col-sm-8">
                                <textarea id="comments" class="form-control" rows="8" placeholder="" name="comment"></textarea>
                                </div>
                              </div>
                              </div>',

        'submit_button' => '<button type="submit" class="button">Отправить</button>',
        'submit_field' => '<div class="form-group"><div class="col-sm-12">%1$s %2$s</div></div></div>',

        'label_submit' => '',
        'fields' => apply_filters('comment_form_default_fields', $fields)
      );
      comment_form($args);
    ?>

    </div>
  </div>
  </div>








  <span class="comments-caption text-right">Всего отзывов: <?php comments_number('%', '%', '%'); ?></span>
    <?php if (get_comments_number() == 0) { ?>
      <ul class="list">
        <li>Еще нет ни одного отзыва. Будь первым!</li>
      </ul>
    <?php } else { ?>
    <ul class="commentlist">
      <?php
        function verstaka_comment($comment, $args, $depth){
          $GLOBALS['comment'] = $comment; ?>
          <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
            <div id="comment-<?php comment_ID(); ?>">
              <div class="comment-author vcard">
                <div class="comment-meta commentmetadata" >
                    <?php printf(__('<span class="fn">%s</span>'), get_comment_author_link()) ?><span><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></span>
                </div>
              </div>
              <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em>
                <br>
              <?php endif; ?>
              <?php comment_text() ?>

            </div>
      <?php }
        $args = array(
          'reply_text' => 'Ответить',
          'callback' => 'verstaka_comment'
        );
        wp_list_comments($args);
      ?>
      <?php if(function_exists('wp_comments_corenavi')) wp_comments_corenavi(); ?>
    </ul>

<!-- Put this div tag to the place, where the Comments block will be -->
<div id="vk_comments"></div>
<script type="text/javascript">
VK.Widgets.Comments("vk_comments", {limit: 10, attach: "*"});
</script>

  </div>
  <?php } ?>
 

  <?php } else { ?>
  <h3>Обсуждения закрыты для данной страницы</h3>
  <?php }
?>